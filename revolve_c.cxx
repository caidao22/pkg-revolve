#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>

using namespace std;

#include "revolve.h"
#include "revolve_c.h"

Revolve *r = NULL;
Revolve *r2 = NULL;

void revolve2_create_offline(int steps,int snaps_in)
{
  if (!r2) {
    r2 = new Revolve(steps,snaps_in);
  }
}

int revolve2_action(int* check,int* capo,int* fine,int snaps_in,int* info,int* where)
{
  enum ACTION::action whatodo;
  int ret   = -1;

  whatodo = r2->revolve(check,capo,fine,snaps_in,info,(bool*)where);
  switch(whatodo) {
    case ACTION::advance:
      ret=1;
      break;
    case ACTION::takeshot:
      if (*where) ret=2;
      else ret=7;
      break;
    case ACTION::firsturn:
      ret=3;
      break;
    case ACTION::youturn:
      ret=4;
      break;
    case ACTION::restore:
      if (*where) ret=5;
      else ret=8;
      break;
    case ACTION::terminate:
      ret=6;
      break;
    case ACTION::error:
      ret=-1;
      break;
  }
  return ret;
}

void revolve2_turn(int final,int* capo,int* fine)
{
  r2->turn(final);
  *capo = r2->getcapo();
  *fine = r2->getfine();
}

void revolve2_reset(void)
{
  if (r2) {
    delete r2;
    r2=NULL;
  }
}

void revolve_create_offline(int steps,int snaps_in)
{
  if (!r) {
    r = new Revolve(steps,snaps_in);
  }
}

void revolve_create_online(int snaps_in)
{
  if (!r) {
    r = new Revolve(snaps_in);
  }
}

void revolve_create_multistage(int steps,int snaps,int snaps_in)
{
  if (!r) {
    r = new Revolve(steps,snaps,snaps_in);
  }
}

int revolve_action(int* check,int* capo,int* fine,int snaps_in,int* info,int* where)
{
  enum ACTION::action whatodo;
  int ret   = -1;

  whatodo = r->revolve(check,capo,fine,snaps_in,info,(bool*)where);
  switch(whatodo) {
    case ACTION::advance:
      ret=1;
      break;
    case ACTION::takeshot:
      if (*where) ret=2;
      else ret=7;
      break;
    case ACTION::firsturn:
      ret=3;
      break;
    case ACTION::youturn:
      ret=4;
      break;
    case ACTION::restore:
      if (*where) ret=5;
      else ret=8;
      break;
    case ACTION::terminate:
      ret=6;
      break;
    case ACTION::error:
      ret=-1;
      break;
  }
  return ret;
}

void revolve_turn(int final,int* capo,int* fine)
{
  r->turn(final);
  *capo = r->getcapo();
  *fine = r->getfine();
}

void revolve_reset(void)
{
  if (r) {
    delete r;
    r=NULL;
  }
}
