include ./make.inc

REVOLVELIB = librevolve.$(AR_LIB_SUFFIX)

lib: revolve.cxx revolve.h revolve_c.cxx revolve_c.h
	$(CXX) -c $(CXXFLAGS) revolve.cxx
	$(CXX) -c $(CXXFLAGS) revolve_c.cxx
	$(AR) $(ARFLAGS) $(REVOLVELIB) revolve.o revolve_c.o
	$(RANLIB) $(REVOLVELIB)

clean:
	$(RM) $(REVOLVELIB) *.o
	
install:
	$(MKDIR) $(PREFIX)/include $(PREFIX)/lib
	$(CP) revolve_c.h $(PREFIX)/include
	$(CP) $(REVOLVELIB) $(PREFIX)/lib
	$(RANLIB) $(PREFIX)/lib/$(REVOLVELIB)

.PHONY:clean
