#ifndef _REVOLVE_C_H
#define _REVOLVE_C_H 1
/*
#ifndef __cplusplus
typedef int bool;
#endif
*/
#ifdef __cplusplus
extern "C" {
#endif
void revolve_create_offline(int,int);
void revolve_create_online(int);
void revolve_create_multistage(int,int,int);
int  revolve_action(int*,int*,int*,int,int*,int*);
void revolve_turn(int,int*,int*);
void revolve_reset(void); 
void revolve2_create_offline(int,int);
int  revolve2_action(int*,int*,int*,int,int*,int*);
void revolve2_turn(int,int*,int*);
void revolve2_reset(void);
#ifdef __cplusplus
}
#endif

#endif /* revolve_c.h */
